const
  gulp = require('gulp'),
  sass = require('gulp-sass'),
  postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer'),
  babel = require('gulp-babel')
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  //browserSync = require('browser-sync').create();

// sass tasks
gulp.task('sass', () =>
  gulp.src('assets/styles/main.scss')
    .pipe(postcss([autoprefixer()]))
    .pipe(sass({
      sourceComments: true,
      outputStyle: 'compressed'
    }))
    .pipe(gulp.dest('dist/styles/'))
    //.pipe(browserSync.stream())
);   

// javascript tasks
gulp.task('js', () =>
  gulp.src('assets/scripts/*')
  .pipe(babel({ presets: ['env'] }))
  .pipe(uglify())
  .pipe(gulp.dest('dist/scripts/'))
);

// sass lint ftw(for better coding practices)
gulp.task('sass_lint', lintCssTask = () => {
  const gulpStylelint = require('gulp-stylelint');
  return gulp
    .src('assets/styles/**/*.scss')
    .pipe(gulpStylelint({
      reporters: [
        { formatter: 'string', console: true }
      ]
    }));
});

// images optimization works with jpeg, jpg, svg, gif, png
gulp.task('images', () =>
  gulp.src('assets/images/*')
    .pipe(imagemin({ verbose: true }))
    .pipe(gulp.dest('dist/images/'))
);
// Just copy/paste html to distribution folder
gulp.task('fonts', () =>
  gulp.src('assets/fonts/*.*')
    .pipe(gulp.dest('dist/fonts/'))
);

// Static Server + watching html/scss/js files
gulp.task('watch_changes', function() {
  gulp.watch('assets/styles/**/*.scss', gulp.series('sass'));
  gulp.watch('assets/scripts/*.js', gulp.series('js'));
  gulp.watch('assets/images/*', gulp.series('images'));
});

gulp.task('prod', gulp.series('sass', 'js', 'images', 'fonts'), () => {
});
gulp.task('watch', gulp.series('watch_changes'));
